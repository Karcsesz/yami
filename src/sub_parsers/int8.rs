use crate::errors::ParseResult;
use crate::mat_data::MatFileData;

/// Parses int8 array
/// ## Parameters
///  - `data`: Data to parse as array
/// ## Return value
/// Returns a `ParseResult` with the parsed data as a `MatFileData::Int8`
pub fn parse_int8(data: &[u8]) -> ParseResult<MatFileData>{
    Ok((&[], MatFileData::Int8(Vec::from_iter(data.iter().map(|data| i8::from_ne_bytes([*data])))))) //NOTE: We don't care about endianness here, so let's use the native one
}

#[cfg(test)]
mod tests {
    use crate::test_utils::to_byte_array_i8;
    use super::*;

    /// Tests parsing a single element
    #[test]
    fn single_element() {
        const DATA: [i8;1] = [
            0x12
        ];

        let byte_array = to_byte_array_i8(&DATA);
        let (extra_data, parsed) = parse_int8(byte_array.as_slice()).unwrap();
        assert_eq!(parsed, MatFileData::Int8(Vec::from(DATA)));
        assert_eq!(extra_data.len(), 0);
    }

    /// Tests parsing a single negative element
    #[test]
    fn single_negative_element() {
        const DATA: [i8;1] = [
            -0x12
        ];

        let byte_array = to_byte_array_i8(&DATA);
        let (extra_data, parsed) = parse_int8(byte_array.as_slice()).unwrap();
        assert_eq!(parsed, MatFileData::Int8(Vec::from(DATA)));
        assert_eq!(extra_data.len(), 0);
    }

    /// Tests parsing every possible i8 element as a large vector
    #[test]
    fn every_possible_element() {
        let data = Vec::from_iter(i8::MIN..i8::MAX);

        let byte_array = to_byte_array_i8(&data);
        let (extra_data, parsed) = parse_int8(byte_array.as_slice()).unwrap();
        assert_eq!(parsed, MatFileData::Int8(data));
        assert_eq!(extra_data.len(), 0);
    }

    /// Tests parsing a lot of elements (123456 bytes)
    #[test]
    fn many_elements() {
        let data = Vec::from_iter((i8::MIN..i8::MAX).cycle().take(123456));

        let byte_array = to_byte_array_i8(&data);
        let (extra_data, parsed) = parse_int8(byte_array.as_slice()).unwrap();
        assert_eq!(parsed, MatFileData::Int8(data));
        assert_eq!(extra_data.len(), 0);
    }
}