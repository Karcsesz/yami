use std::io::Read;
use flate2::read::ZlibDecoder;
use log::{debug, trace, warn};
use crate::errors::MatError::Decompression;
use crate::errors::{ParseResult};
use crate::fail;
use crate::mat_data::MatFileData;
use crate::sub_parsers::parse_single_data_field;

//TODO: Alignment with multiple compressed fields? (spec. page 1-6 [12])

pub fn parse_compressed(data: &[u8], big_endian: bool) -> ParseResult<MatFileData>{
    let mut decoder = ZlibDecoder::new(data);
    let mut decoded = Vec::new();
    let (unprocessed, parsed) = match decoder.read_to_end(&mut decoded) {
        Ok(ok) => {
            debug!("Decompressed {} bytes to {} bytes", data.len(), ok);
            parse_single_data_field(decoded.as_slice(), big_endian)?
        }
        Err(e) => fail!(Decompression(e))
    };

    if unprocessed.len() > 0 {
        warn!("{} bytes of unprocessed data at the end of compressed block", unprocessed.len());
        trace!("{:?}", unprocessed);
    }

    Ok((&[], parsed))
}

//TODO: unit tests