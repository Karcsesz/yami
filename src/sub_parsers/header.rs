use crate::errors::ParseResult;
use crate::fail;
use crate::errors::MatError::*;
use crate::mat_data::HeaderContents;
use crate::sub_parsers::parser_utils::take;

/// Parses header of MAT file
/// ## Parameters
///  - `input`: Complete contents of MAT file
/// ## Return value
/// Returns a `ParseResult` with the `HeaderContents` and the rest of the file
/// ## Errors
/// Returns an error if:
///  - The header's text field isn't valid UTF-8 (`CorruptHeaderText` with the `FromUtf8Error` wrapped)
///  - The MAT file doesn't start as a v5 MAT file should (`UnsupportedLevel`)
///  - The endian marker is corrupted (`CorruptEndianMarker`)
///  - There's data in the subsystem field of the header (`SubsystemDataFound`)
pub fn parse_header(input: &[u8]) -> ParseResult<HeaderContents> {
    // Extract header data
    let (header, extra) = take(input, 128)?;
    let header: [u8;128] = header.try_into().unwrap(); //NOTE: Shouldn't ever panic because take() guarantees 128 bytes. There has to be a better way to do this

    // Partition header data
    let text = match String::from_utf8(Vec::from(&header[..116])) {
        Ok(data) => data,
        Err(e) => fail!(CorruptHeaderText(e))
    };
    let subsystem = Vec::from(&header[116..124]);
    let version = [
        header[124],
        header[125]
    ];
    let endian = u16::from_le_bytes([header[126], header[127]]);

    // Check if the file begins as it should (according to MATfile v5 spec)
    if let Some(_) = text.bytes().take(4).find(|data| data == &0x00) {
        fail!(UnsupportedLevel)
    }

    // Check if we need to swap endianness
    let big_endian = if endian == 0x4d49u16 {
        false
    } else if endian == 0x494du16 {
        true
    } else {
        fail!(CorruptEndianMarker)
    };

    // Read version data
    let version = if big_endian {
        u16::from_be_bytes(version)
    } else {
        u16::from_le_bytes(version)
    };

    // Make sure there's no subsystem data
    //TODO: Maybe we shouldn't fail on this and just let the library user handle it? What even is this data?
    if let Some(_) = subsystem.iter().find(|&data| data != &0x00 && data != &0x20) {
        fail!(SubsystemDataFound)
    }

    // Build header struct
    let contents = HeaderContents {
        text: String::from_utf8(Vec::from(text)).unwrap_or("Failed to decode header text!".to_string()), //TODO: Propagate this error properly
        subsystem_data_offset: Vec::from(subsystem),
        version,
        needs_endian_swapping: big_endian
    };

    Ok((extra, contents))
}

#[cfg(test)]
mod tests {
    use log::info;
    use super::*;

    const VALID_HEADER: [u8;128] = [
        0x4d, 0x41, 0x54, 0x4c,
        0x41, 0x42, 0x20, 0x35,
        0x2e, 0x30, 0x20, 0x4d,
        0x41, 0x54, 0x2d, 0x66,
        0x69, 0x6c, 0x65, 0x2c,
        0x20, 0x50, 0x6c, 0x61,
        0x74, 0x66, 0x6f, 0x72,
        0x6d, 0x3a, 0x20, 0x57,
        0x69, 0x6e, 0x33, 0x32,
        0x4e, 0x54, 0x2c, 0x20,
        0x43, 0x52, 0x45, 0x41,
        0x54, 0x45, 0x44, 0x20,
        0x6f, 0x6e, 0x3a, 0x20,
        0x54, 0x75, 0x65, 0x2c,
        0x20, 0x31, 0x38, 0x20,
        0x4f, 0x63, 0x74, 0x20,
        0x32, 0x30, 0x32, 0x32,
        0x20, 0x31, 0x34, 0x3a,
        0x34, 0x34, 0x3a, 0x35,
        0x37, 0x20, 0x47, 0x4d,
        0x54, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x01, 0x49, 0x4d
    ];

    fn validate_contents(contents: HeaderContents) {
        assert_eq!(contents.text, "MATLAB 5.0 MAT-file, Platform: Win32NT, CREATED on: Tue, 18 Oct 2022 14:44:57 GMT\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0");
        assert_eq!(contents.version, 0x0100);
        assert_eq!(contents.needs_endian_swapping, false);
        assert_eq!(contents.subsystem_data_offset, vec!(0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00));
    }

    /// Tests what happens with a file truncated at the header section
    #[test]
    #[should_panic]
    fn header_too_short() {
        let bad_header = &VALID_HEADER[..VALID_HEADER.len()-1];
        info!("Attempting to parse header. Data:\n{:?}", bad_header);
        parse_header(bad_header).unwrap();
    }

    /// Tests what happens if there's subsystem data in the header
    #[test]
    #[should_panic]
    fn header_contains_subsys_spec_data() {
        let mut bad_header = VALID_HEADER;
        bad_header[116] = 0x12;
        parse_header(&bad_header).unwrap();
    }

    /// Tests what happens if the MAT file isn't to v5 spec
    #[test]
    #[should_panic]
    fn file_level_mismatch() {
        let mut bad_header = VALID_HEADER;
        bad_header[3] = 0x00;
        parse_header(&bad_header).unwrap();
    }

    /// Tests parsing the test header without corruptions
    #[test]
    fn valid_header() {
        let (extra, parsed) = parse_header(&VALID_HEADER).unwrap();
        assert_eq!(extra.len(), 0);
        validate_contents(parsed);
    }

    /// Makes sure that the header parser doesn't truncate the file body
    #[test]
    fn valid_header_with_body() {
        let header = VALID_HEADER.repeat(2);
        let (extra, parsed) = parse_header(header.as_slice()).unwrap();
        assert_eq!(extra, VALID_HEADER.as_slice());
        validate_contents(parsed);
    }

    /// Checks if endian swapping is detected correctly
    #[test]
    fn endian_swap() {
        let mut bad_header = VALID_HEADER;
        bad_header.swap(126usize,127usize);
        assert!(parse_header(&bad_header).unwrap().1.needs_endian_swapping);
    }

    /// Tests what happens with a corrupt endian marker
    #[test]
    #[should_panic]
    fn corrupt_endian_marker() {
        let mut bad_header = VALID_HEADER;
        bad_header.swap(125usize,126usize);
        parse_header(&bad_header).unwrap();
    }
}