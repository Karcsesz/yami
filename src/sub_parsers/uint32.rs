use crate::errors::ParseResult;
use crate::mat_data::MatFileData;

/// Parses uint32 array with selectable endianness
/// ## Parameters
///  - `data`: Data to parse as array
///  - `big_endian`: Set to true if the data should be parsed as big endian
/// ## Return value
/// Returns a `ParseResult` with the parsed data as a `MatFileData::UInt32`,
/// and the remaining bytes set to the last 0-3 bytes of the input data
/// if there are extra bytes at the end of the array.
pub fn parse_uint32(data: &[u8], big_endian: bool) -> ParseResult<MatFileData>{
    // Chunk the data
    let data = data.chunks_exact(4);

    // If there are leftover bytes, put them away for later
    let remainder = data.remainder();

    // Select parser for the correct endianness
    let parser = if big_endian {
        u32::from_be_bytes
    } else {
        u32::from_le_bytes
    };

    // Parse every chunk with the selected parser
    let parsed = MatFileData::UInt32(data.map(
        |chunk| {
            parser([chunk[0], chunk[1], chunk[2], chunk[3]])
        }
    ).collect());

    Ok((remainder, parsed))
}

#[cfg(test)]
mod tests {
    use crate::test_utils::{to_byte_array_u32};
    use super::*;

    /// Tests parsing a single element
    #[test]
    fn single_element() {
        const DATA: [u32;1] = [
            0x12345678u32
        ];

        let byte_array = to_byte_array_u32(&DATA);
        let (extra_data, parsed) = parse_uint32(byte_array.as_slice(), false).unwrap();
        assert_eq!(parsed, MatFileData::UInt32(Vec::from(DATA)));
        assert_eq!(extra_data.len(), 0);
    }

    /// Tests what happens if there isn't enough data to form an element
    #[test]
    fn not_enough_data() {
        const DATA: [u8;3] = [
            0x12, 0x34, 0x56
        ];

        let parsed = parse_uint32(&DATA, false).unwrap();
        assert_eq!(parsed.0, DATA);
        assert_eq!(parsed.1, MatFileData::UInt32(vec![]));
    }

    /// Tests what happens if there isn't enough data to form an element with `big_endian` set to true
    #[test]
    fn not_enough_data_reverse_endian() {
        const DATA: [u8;3] = [
            0x12, 0x34, 0x56
        ];

        let parsed = parse_uint32(&DATA, true).unwrap();
        assert_eq!(parsed.0, DATA);
        assert_eq!(parsed.1, MatFileData::UInt32(vec![]));
    }

    /// Tests what happens if there are 3 unparseable bytes left at the end of the array
    #[test]
    fn too_short_element() {
        const DATA: [u8;7] = [
            0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD
        ];

        let (extra_data, parsed) = parse_uint32(&DATA, false).unwrap();

        assert_eq!(extra_data.len(), 3);
        assert_eq!(parsed, MatFileData::UInt32(vec!(u32::from_le(0x67452301u32))));
    }

    /// Tests what happens if there are 3 unparseable bytes left at the end of the array with `big_endian` set to true
    #[test]
    fn too_short_element_reverse_endian() {
        const DATA: [u8;7] = [
            0x67, 0x45, 0x23, 0x01, 0x89, 0xAB, 0xCD
        ];

        let (extra_data, parsed) = parse_uint32(&DATA, true).unwrap();

        assert_eq!(extra_data.len(), 3);
        assert_eq!(parsed, MatFileData::UInt32(vec!(0x67452301u32)));
    }
}