use std::fs::File;
use std::path::Path;
use log::{debug, info, trace};
use crate::errors::{MatFileError};
use crate::errors::MatError::{LoadError, OpenError};
use crate::mat_data::{Contents};
use crate::sub_parsers::{parse_data_fields};

mod sub_parsers;
pub mod errors;
pub mod mat_data;
#[cfg(test)]
mod test_utils;

/// Decode a byte array as MAT file
/// ## Parameters
///  - `data`: Byte array to parse as MAT file
/// ## Return value
/// A `Contents` struct with the MAT file's contents
/// ## Errors
/// Returns a `MatFileError` if parsing fails at any point during the process
/// ## Panics
/// Currently panics if there are malformed strings in some places in the data. Will be removed.
pub fn decode(data: &[u8]) -> Result<Contents, MatFileError> {
    let (extra, header) = sub_parsers::parse_header(&data)?;
    debug!("Parsed header");
    trace!("{:?}", header);

    let body = parse_data_fields(extra, header.needs_endian_swapping)?;

    info!("Parsed MAT file.");
    Ok(Contents {header, body})
}

/// Load `file` into memory and decode it as a MAT file (using `decode`)
/// ## Parameters
///  - `file`: A struct implementing `std::io::Read` serving as the MAT file stream
/// ## Return value
/// A `Contents` struct with the MAT file's contents
/// ## Errors
/// Returns a `MatFileError` if parsing fails at any point during the process
/// ## Panics
/// Currently panics if there are malformed strings in some places in the data. Will be removed.
pub fn load<R: std::io::Read>(mut file: R) -> Result<Contents, MatFileError> {
    let mut data = Vec::<u8>::new();
    if let Err(e) = file.read_to_end(&mut data) {
        fail!(LoadError(e))
    }
    info!("Read MAT file data from stream, {} bytes.", data.len());
    decode(data.as_slice())
}

/// Load file at `path` into memory and decode it as a MAT file (using `decode`)
/// ## Parameters
///  - `path`: Path of file to decode
/// ## Return value
/// A `Contents` struct with the MAT file's contents
/// ## Errors
/// Returns a `MatFileError` if parsing fails at any point during the process
/// ## Panics
/// Currently panics if there are malformed strings in some places in the data. Will be removed.
pub fn open<P: AsRef<Path>>(path: P) -> Result<Contents, MatFileError> {
    let file = match File::open(path) {
        Ok(file) => file,
        Err(error) => fail!(OpenError(error))
    };
    info!("Opened MAT file for reading");
    debug!("{:?}", file);
    load(file)
}