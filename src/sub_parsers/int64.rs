use crate::errors::ParseResult;
use crate::mat_data::MatFileData;

/// Parses int64 array with selectable endianness
/// ## Parameters
///  - `data`: Data to parse as array
///  - `big_endian`: Set to true if the data should be parsed as big endian
/// ## Return value
/// Returns a `ParseResult` with the parsed data as a `MatFileData::Int64`,
/// and the remaining bytes set to the last 0-7 bytes of the input data
/// if there are extra bytes at the end of the array.
pub fn parse_int64(data: &[u8], big_endian: bool) -> ParseResult<MatFileData>{
    // Chunk data
    let data = data.chunks_exact(8);

    // If there are leftover bytes, put them away for later
    let remainder = data.remainder();

    // Select parser based on endianness
    let parser = if big_endian {
        i64::from_be_bytes
    } else {
        i64::from_le_bytes
    };

    // Parse every chunk
    let parsed = MatFileData::Int64(data.map(
    |chunk| {
            parser([chunk[0], chunk[1], chunk[2], chunk[3], chunk[4], chunk[5], chunk[6], chunk[7]])
        }
    ).collect());

    Ok((remainder, parsed))
}

#[cfg(test)]
mod tests {
    use crate::test_utils::to_byte_array_i64;
    use super::*;

    /// Tests parsing a single element
    #[test]
    fn single_element() {
        const DATA: [i64;1] = [
            0x123456789ABCDEFi64
        ];

        let byte_array = to_byte_array_i64(&DATA);
        let (extra_data, parsed) = parse_int64(byte_array.as_slice(), false).unwrap();
        assert_eq!(parsed, MatFileData::Int64(Vec::from(DATA)));
        assert_eq!(extra_data.len(), 0);
    }

    /// Tests what happens if there aren't enough bytes to form an element
    #[test]
    fn not_enough_data() {
        const DATA: [u8;7] = [
            0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE
        ];

        let parsed = parse_int64(&DATA, false).unwrap();
        assert_eq!(parsed.0, DATA);
        assert_eq!(parsed.1, MatFileData::Int64(vec![]));
    }

    /// Tests what happens if there aren't enough bytes to form an element with `big_endian` set to true
    #[test]
    fn not_enough_data_reverse_endian() {
        const DATA: [u8;7] = [
            0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE
        ];

        let parsed = parse_int64(&DATA, true).unwrap();
        assert_eq!(parsed.0, DATA);
        assert_eq!(parsed.1, MatFileData::Int64(vec![]));
    }

    /// Tests what happens if there are leftover bytes
    #[test]
    fn too_short_element() {
        const DATA: [u8;15] = [
            0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0x1F,
            0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD
        ];

        let (extra_data, parsed) = parse_int64(&DATA, false).unwrap();

        assert_eq!(extra_data.len(), 7);
        assert_eq!(parsed, MatFileData::Int64(vec!(0x1FCDAB8967452301i64)));
    }

    /// Tests what happens if there are leftover bytes with `big_endian` set to true
    #[test]
    fn too_short_element_reverse_endian() {
        const DATA: [u8;15] = [
            0x1F, 0xCD, 0xAB, 0x89, 0x67, 0x45, 0x23, 0x01,
            0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD
        ];

        let (extra_data, parsed) = parse_int64(&DATA, true).unwrap();

        assert_eq!(extra_data.len(), 7);
        assert_eq!(parsed, MatFileData::Int64(vec!(0x1FCDAB8967452301i64)));
    }
}