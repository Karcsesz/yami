use std::collections::HashMap;
use std::iter::zip;
use log::{debug, error, trace, warn};
use num_traits::ToPrimitive;
use crate::errors::MatError::{InvalidDataType, InvalidLength, TooShort, Unimplemented};
use crate::errors::{ParseResult};
use crate::fail;
use crate::mat_data::{MatFileData, MatlabString, MatlabNumericArray, MatlabStruct, MatlabStringType, MatlabCharacterArrayExtras, MatlabCellArray};
use crate::mat_data::MatFileData::{UInt16};
use crate::sub_parsers::{parse_data_fields, parse_single_data_field};
use crate::sub_parsers::parser_utils::parse_string;

fn parse_cell_array(data: &[u8], array_flags: [u32;2], big_endian: bool) -> ParseResult<MatFileData> {
    debug!("Processing matlab cell array");
    let (data, dimensions) = parse_single_data_field(data, big_endian)?;
    let (data, name) = parse_single_data_field(data, big_endian)?;
    let cells = parse_data_fields(data, big_endian)?;
    debug!("Finished partitioning cell data.");

    let sizes = match dimensions {
        MatFileData::Int32(data) => data,
        invalid => {
            let error_message = format!("cell dimennsions size is invalid type: {:?}", invalid);
            error!("{}", error_message);
            fail!(InvalidDataType);
        }
    };

    let name = match name {
        MatFileData::Int8(data) => data,
        invalid => {
            let error_message = format!("cell array name is invalid type: {:?}", invalid);
            error!("{}", error_message);
            fail!(InvalidDataType);
        }
    };

    let name = parse_string(name);
    debug!("Cell array name is {name}");

    Ok((&[], MatFileData::MatlabCellArray(MatlabCellArray {
        array_flags,
        sizes,
        name,
        cells,
    })))
}

fn parse_struct(data: &[u8], array_flags: [u32;2], big_endian: bool) -> ParseResult<MatFileData> {
    debug!("Processing matlab struct");
    let (data, dimensions) = parse_single_data_field(data, big_endian)?;
    let (data, name) = parse_single_data_field(data, big_endian)?;
    let (data, field_name_length) = parse_single_data_field(data, big_endian)?;
    let (data, field_names) = parse_single_data_field(data, big_endian)?;
    let fields = parse_data_fields(data, big_endian)?;
    debug!("Finished partitioning struct data.");

    let sizes = match dimensions {
        MatFileData::Int32(data) => data,
        invalid => {
            let error_message = format!("matrix dimensions size is invalid type: {:?}", invalid);
            error!("{}", error_message);
            fail!(InvalidDataType);
        }
    };

    let name = match name {
        MatFileData::Int8(data) => data,
        invalid => {
            let error_message = format!("matrix name is invalid type: {:?}", invalid);
            error!("{}", error_message);
            fail!(InvalidDataType);
        }
    };

    let name = parse_string(name);
    debug!("Struct name is {name}");

    let field_name_length = if let MatFileData::Int32(num_array) = field_name_length {
        if num_array.len() > 1 {
            warn!("Struct field name length field is array with {} elements! Taking only element 0", num_array.len());
            trace!("{:?}", num_array);
        }
        match num_array.get(0) {
            Some(elem) => {elem.clone()}
            None => {
                error!("Field name length is 0");
                fail!(TooShort);
            }
        }
    } else {
        error!("Invalid field name length datatype: {:?}", field_name_length);
        fail!(InvalidDataType);
    };

    let field_names: Vec<String> = if let MatFileData::Int8(field_names) = field_names {
        let field_name_length = field_name_length.to_usize().expect("Bloody MATLAB storing lengths as a signed value..."); //TODO: Less snark, more error handling
        if field_names.len() % field_name_length != 0 {
            warn!("Field names array length not a multiple of single field name length.");
            warn!("Array length: {}, field length: {}", field_names.len(), field_name_length);
        }
        field_names.chunks(field_name_length)
            .map(|chunk|
                String::from_utf8(chunk.iter().filter_map(
                    |character| if character != &0 {Some(character.to_u8().expect("Non-ASCII string in field name"))} else { None } //TODO: Propagate error instead of panicking
                ).collect()).expect("Failed to build string from field name")
            ).collect()
    } else {
        error!("Invalid field name datatype: {:?}", field_names);
        fail!(InvalidDataType);
    };
    debug!("Parsed struct with field names {:?}", field_names);
    let fields: Vec<HashMap<String,MatFileData>> = fields.chunks(field_names.len()).map(|chunk| {
        let chunk = chunk.to_vec();
        HashMap::from_iter( zip(field_names.clone(), chunk))
    }
    ).collect();
    //TODO: Make sure every element got parsed
    Ok((&[], MatFileData::MatlabStruct(
        MatlabStruct {
            array_flags,
            sizes,
            name,
            field_name_length,
            fields
        }
    )))
}

fn parse_character_array(data: &[u8], array_flags: [u32;2], big_endian: bool) -> ParseResult<MatFileData> {
    debug!("Decoding character array using numeric array subparser");
    let numeric = parse_numeric_matrix(data, array_flags, big_endian)?; //TODO: Maybe some special postprocessing?
    if let MatFileData::MatlabNumericArray(array) = numeric.1 {
        let string = match *array.real {
            UInt16(string) => String::from_utf16_lossy(&string),
            MatFileData::MatlabString(string) => string.data,
            datatype => {
                error!("Character matrix internal datatype is {:?}", datatype);
                fail!(InvalidDataType)
            }
        };
        Ok((
            numeric.0,
            MatFileData::MatlabString(
                MatlabString {
                    subtype: MatlabStringType::CharacterArray(
                        MatlabCharacterArrayExtras {
                            array_flags: array.array_flags,
                            sizes: array.sizes,
                            name: array.name,
                        }
                    ),
                    data: string
                }
            ))
        )
    } else {
        error!("Character matrix decoded as {:?}", numeric.1);
        fail!(InvalidDataType)
    }
}

fn parse_numeric_matrix(data: &[u8], array_flags: [u32;2], big_endian: bool) -> ParseResult<MatFileData> {
    debug!("Decoding numeric matrix");
    let (data, dimensions) = parse_single_data_field(data, big_endian)?;
    let (data, name) = parse_single_data_field(data, big_endian)?;
    let (data, real) = parse_single_data_field(data, big_endian)?;
    let has_imaginary = array_flags[0] & 0x08 << 8 != 0;
    let imaginary = if has_imaginary {
        debug!("Numeric matrix has imaginary field");
        let (extra, imaginary) = parse_single_data_field(data, big_endian)?;
        if extra.len() > 0 {
            warn!("{} bytes of data left over after matrix imaginary element", extra.len());
            trace!("{:?}", extra);
        }
        Some(imaginary)
    } else {
        if data.len() > 0 {
            warn!("{} bytes of data left over after matrix real element, but the imaginary bit is not set", data.len());
        }
        None
    };

    //TODO: Make sure underlying datatype matches found type
    //TODO: Logging for errors

    let sizes = match dimensions {
        MatFileData::Int32(data) => data,
        _invalid => fail!(InvalidDataType)
    };

    let name = match name {
        MatFileData::Int8(data) => data,
        _invalid => fail!(InvalidDataType)
    };
    let name = parse_string(name);

    //TODO: Zip imaginary values?
    Ok((&[], MatFileData::MatlabNumericArray( MatlabNumericArray {
        array_flags,
        sizes,
        name,
        real: Box::new(real),
        imaginary: imaginary.and_then(|data| Some(Box::new(data)))
    })))
}

pub fn parse_matrix_data(data: &[u8], big_endian: bool) -> ParseResult<MatFileData> {
    debug!("About to parse matrix header");
    let (extra, array_flags) = parse_single_data_field(data, big_endian)?;
    let array_flags = match array_flags {
        MatFileData::UInt32(data) => data,
        _invalid => fail!(InvalidDataType)
    };

    let array_flags = match (|array_flags: Vec<u32>| -> Option<[u32;2]> {Some([array_flags.get(0)?.clone(), array_flags.get(1)?.clone()])})(array_flags) {
        None => fail!(InvalidLength),
        Some(flags) => flags
    };
    let array_class = array_flags[0].to_le_bytes()[0];

    debug!("About to process Matlab matrix of class {}", array_class);
    match array_class {
        1 => parse_cell_array(extra, array_flags, big_endian),
        2 => parse_struct(extra, array_flags, big_endian),
        4 => parse_character_array(extra, array_flags, big_endian),
        6|7|8|9|10|11|12|13|14|15 => parse_numeric_matrix(extra, array_flags, big_endian),
        unk => {
            let error_message = format!("Unknown array class {}", unk);
            error!("{}", error_message);
            fail!(Unimplemented(error_message));
        }
    }
}

#[cfg(test)]

mod tests {
    use super::*;

    const VALID_IMAGINARY_MATRIX: [u8;128] = [
                //0x0e, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, // Matrix header for 128 byte matrix
                0x06, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, // Array flags
                0x06, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x05, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, // Dimensions array
                0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,
                0x01, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, // Array name
                0x6d, 0x79, 0x5f, 0x61, 0x72, 0x72, 0x61, 0x79, // my_array
                0x09, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, // Double x4
                0x9a, 0x99, 0x99, 0x99, 0x99, 0x99, 0xF1, 0x3F, // 1.1
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x40, // 3.0
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, // 2.0
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x40, // 4.0
                0x09, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, // Double x4 (imaginary part)
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 0x3F, // 1.0
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 0.0
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 0.0
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 0.0
    ];

    //TODO: Matrix without imaginary section
    //TODO: Corrupt matrices and failure modes
    //TODO: Cell arrays

    #[test]
    fn valid_matrix_with_imaginary() {
        let (extra, parsed) = parse_matrix_data(&VALID_IMAGINARY_MATRIX, false).unwrap();
        assert_eq!(extra.len(), 0);
        match parsed {
            MatFileData::MatlabNumericArray(array) => {
                assert!(array.imaginary.is_some());
                assert_eq!(array.imaginary, Some(Box::new(MatFileData::Double(vec![1.0,0.0,0.0,0.0]))));
                assert_eq!(array.real, Box::new(MatFileData::Double(vec![1.1,3.0,2.0,4.0])));
                //assert_eq!(array.array_flags, ) //TODO
                assert_eq!(array.name, "my_array");
                assert_eq!(array.sizes, vec![2,2]);
            },
            invalid => panic!("MatlabArray parsed as {:?}", invalid)
        }
    }
}