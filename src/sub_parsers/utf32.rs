use crate::errors::MatError::{InvalidDataType, UTF32ParseError};
use crate::errors::ParseResult;
use crate::fail;
use crate::mat_data::{MatFileData, MatlabString};
use crate::mat_data::MatlabStringType::UTF32;
use crate::sub_parsers::uint32::parse_uint32;

pub fn parse_utf32(data: &[u8], big_endian: bool) -> ParseResult<MatFileData> {
    let (extra, data) = parse_uint32(data, big_endian)?;
    if let MatFileData::UInt32(data) = data {
        let mut results = String::with_capacity(data.len());
        for character in data {
            results.push(match char::from_u32(character) {
                None => fail!(UTF32ParseError),
                Some(character) => character
            });
        };

        Ok((extra, MatFileData::MatlabString( MatlabString {
            data: results,
            subtype: UTF32
        }
        )))
    } else {
        fail!(InvalidDataType)
    }
}

//TODO: Testing