use std::collections::HashMap;

/// MAT file data field
#[derive(Debug, PartialEq, Clone)]
pub enum MatFileData {
    Int8(Vec<i8>),
    UInt8(Vec<u8>),
    Int16(Vec<i16>),
    UInt16(Vec<u16>),
    Int32(Vec<i32>),
    UInt32(Vec<u32>),
    Int64(Vec<i64>),
    Single(Vec<f32>),
    Double(Vec<f64>),
    MatlabCellArray(MatlabCellArray),
    MatlabStruct(MatlabStruct),
    MatlabObject(MatlabObject),
    MatlabNumericArray(MatlabNumericArray),
    MatlabString(MatlabString),
}

///TODO: Cell array datatype
#[derive(Debug, PartialEq, Clone)]
pub struct MatlabCellArray {
    pub array_flags: [u32;2],
    pub sizes: Vec<i32>,
    pub name: String,
    pub cells: Vec<MatFileData>
}

/// Matlab struct
/// TODO: how do multidimensional structs work?
#[derive(Debug, PartialEq, Clone)]
pub struct MatlabStruct {
    pub array_flags: [u32;2],
    pub sizes: Vec<i32>,
    pub name: String,
    pub field_name_length: i32,
    pub fields: Vec<HashMap<String, MatFileData>>
}

///TODO: Matlab object
#[derive(Debug, PartialEq, Clone)]
pub struct MatlabObject {
    pub array_flags: [u32;2]
}

/// Matlab string
#[derive(Debug, PartialEq, Clone)]
pub struct MatlabString {
    pub subtype: MatlabStringType,
    pub data: String,
}

/// Matlab string extra data (for MatlabCharacterArray)
#[derive(Debug, PartialEq, Clone)]
pub enum MatlabStringType {
    CharacterArray(MatlabCharacterArrayExtras),
    UTF8,
    UTF16,
    UTF32
}

/// Extra fields added by MatlabCharacterArray
#[derive(Debug, PartialEq, Clone)]
pub struct MatlabCharacterArrayExtras {
    pub array_flags: [u32;2],
    pub sizes: Vec<i32>,
    pub name: String
}

///TODO: Sparse array
#[derive(Debug, PartialEq, Clone)]
pub struct MatlabSparseArray {
    pub array_flags: [u32;2]
}

/// Matlab numeric array
#[derive(Debug, PartialEq, Clone)]
pub struct MatlabNumericArray {
    pub array_flags: [u32;2],
    pub sizes: Vec<i32>,
    pub name: String,
    pub real: Box<MatFileData>,
    pub imaginary: Option<Box<MatFileData>>
}

/// Main struct returned by the library.
/// - `header`: Contents of MAT file header
/// - `body`: Vector of data fields in MAT file
#[derive(Debug, PartialEq, Clone)]
pub struct Contents {
    pub header: HeaderContents,
    pub body: Vec<MatFileData>
}

/// MAT file header contents
#[derive(Debug, PartialEq, Clone)]
pub struct HeaderContents {
    pub text: String,
    pub subsystem_data_offset: Vec<u8>,
    pub version: u16,
    pub needs_endian_swapping: bool
}
