# YAMI
> Yet Another Matfile ~~Interface~~ Importer

YAMI is an in-development Rust library to work with MATLAB MAT files. Currently only reading is supported.

:warning: __TEST FILES WANTED__ :warning:
## Features
 - Level 5 compressed or uncompressed MAT files only
 - Both native and reverse endianness
 - Low level access to MAT file contents

## Current Data Type Support
Importing: 
 - [x] Integer types
 - [x] Floating point types
 - [x] Cell arrays
 - [x] MATLAB structures
 - [ ] MATLAB objects
 - [x] Character arrays
 - [ ] Sparse matrices
 - [x] Floating point arrays
 - [x] Integer arrays
 - [x] Compressed data types
 - [x] Strings

Exporting:
 - Currently not implemented
## Planned features
 - High level API
 - Writing of MAT files
 - Transparent passthrough of subsystem-specific data