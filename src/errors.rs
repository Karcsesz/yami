use std::backtrace::Backtrace;
use std::io;
use std::string::{FromUtf16Error, FromUtf8Error};

pub type ParseResult<'a, O> = Result<(&'a[u8], O), MatFileError>;

#[derive(Debug)]
pub struct MatFileError {
    pub trace: Backtrace, // Backtrace from the source of the error
    pub error: MatError, // Type of error
}

#[derive(Debug)]
pub enum MatError {
    OpenError(io::Error), // Failed to open file
    LoadError(io::Error), // Failed to load file
    TooShort, // File is too short
    CorruptHeaderText(FromUtf8Error), // Header text field is invalid
    UnsupportedLevel, // File isn't to v5 spec
    CorruptEndianMarker, // Endian marker in header is corrupt
    SubsystemDataFound, // Header subsystem field isn't empty
    Unimplemented(String), // Given feature is currently unimplemented
    UnknownDatatype(String), // Unknown datatype
    InvalidDataType, // Subfield of data field has invalid datatype
    InvalidLength, // Subfield of data field has invalid length
    Decompression(io::Error), // Decompression of compressed data field failed
    UTF8ParseError(FromUtf8Error), // Failed to parse UTF-8 string
    UTF16ParseError(FromUtf16Error), // Failed to parse UTF-16 string
    UTF32ParseError // Failed to parse UTF-32 string (invalid character)
}

/// Returns a `MatFileError` with backtrace and given error type
#[macro_export] macro_rules! fail {
    ($err:expr) => {
        return Err(crate::errors::MatFileError{error: $err.into(), trace: std::backtrace::Backtrace::capture()})
    };
}