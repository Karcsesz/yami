use crate::errors::ParseResult;
use crate::mat_data::MatFileData;

/// Parses uint8 array
/// ## Parameters
///  - `data`: Data to parse as array
/// ## Return value
/// Returns a `ParseResult` with the parsed data as a `MatFileData::UInt8`
pub fn parse_uint8(data: &[u8]) -> ParseResult<MatFileData>{
    Ok((&[], MatFileData::UInt8(Vec::from(data))))
}

#[cfg(test)]
mod tests {
    use super::*;

    /// Tests parsing a single element
    #[test]
    fn single_element() {
        const DATA: [u8;1] = [
            0x12
        ];
        let (extra_data, parsed) = parse_uint8(&DATA).unwrap();
        assert_eq!(parsed, MatFileData::UInt8(Vec::from(DATA)));
        assert_eq!(extra_data.len(), 0);
    }

    /// Tests parsing multiple elements
    #[test]
    fn multiple_element() {
        const DATA: [u8;8] = [
            0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF
        ];
        let (extra_data, parsed) = parse_uint8(&DATA).unwrap();
        assert_eq!(parsed, MatFileData::UInt8(Vec::from(DATA)));
        assert_eq!(extra_data.len(), 0);
    }

    /// Tests parsing every possible u8 element as a large vector
    #[test]
    fn every_possible_element() {
        let data = Vec::from_iter(u8::MIN..u8::MAX);

        let (extra_data, parsed) = parse_uint8(data.as_slice()).unwrap();
        assert_eq!(parsed, MatFileData::UInt8(data.clone()));
        assert_eq!(extra_data.len(), 0);
    }

    /// Tests parsing a lot of elements (123456 bytes)
    #[test]
    fn many_elements() {
        let data = Vec::from_iter((u8::MIN..u8::MAX).cycle().take(123456));

        let (extra_data, parsed) = parse_uint8(data.as_slice()).unwrap();
        assert_eq!(parsed, MatFileData::UInt8(data.clone()));
        assert_eq!(extra_data.len(), 0);
    }
}