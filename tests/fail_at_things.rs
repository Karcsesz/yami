
#[test]
#[should_panic]
fn empty_path() {
    yami::open("").unwrap();
}

#[test]
#[should_panic]
fn open_this_source_file() {
    yami::open(file!()).unwrap();
}

//TODO: Crash mid-load with yami::load()