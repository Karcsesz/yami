use crate::errors::MatError::{InvalidDataType, UTF16ParseError};
use crate::errors::ParseResult;
use crate::fail;
use crate::mat_data::{MatFileData, MatlabString};
use crate::mat_data::MatlabStringType::UTF16;
use crate::sub_parsers::uint16::parse_uint16;

pub fn parse_utf16(data: &[u8], big_endian: bool) -> ParseResult<MatFileData> {
    let (extra, data) = parse_uint16(data, big_endian)?;
    if let MatFileData::UInt16(data) = data {
        Ok((extra, MatFileData::MatlabString(MatlabString {
            data: match String::from_utf16(&data) {
                Ok(data) => data,
                Err(err) => fail!(UTF16ParseError(err))
            },
            subtype: UTF16
        })))
    } else {
        fail!(InvalidDataType)
    }
}

//TODO: Testing