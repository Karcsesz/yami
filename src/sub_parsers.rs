mod header;

pub use header::parse_header;

mod matrix;
mod uint32;
mod int32;
mod int8;
mod double;
mod uint8;
mod single;
mod int64;
mod uint16;
mod int16;
mod small_data_element;
mod parser_utils;
mod compressed;
mod utf8;
mod utf16;
mod utf32;

use log::{debug, error, trace, warn};
use crate::mat_data::MatFileData;
use double::parse_double;
use int16::parse_int16;
use int32::parse_int32;
use int64::parse_int64;
use int8::parse_int8;
use matrix::parse_matrix_data;
use single::parse_single;
use uint16::parse_uint16;
use uint32::parse_uint32;
use uint8::parse_uint8;
use crate::fail;
use crate::errors::{MatFileError, ParseResult};
use crate::errors::MatError::*;
use crate::sub_parsers::compressed::parse_compressed;
use crate::sub_parsers::small_data_element::parse_small;
use crate::sub_parsers::parser_utils::{take, uint32};
use crate::sub_parsers::utf16::parse_utf16;
use crate::sub_parsers::utf32::parse_utf32;
use crate::sub_parsers::utf8::parse_utf8;

/// Parses multiple data fields in succession
/// ## Parameters
///  - `data`: Data array to parse fields from
///  - `big_endian`: Set to true if fields should be parsed as big endian
/// ## Return value
/// A `Vec<MatFileData>` with the decoded fields
/// ## Errors
/// Returns an error if there's an issue during parsing
pub fn parse_data_fields(mut data: &[u8], big_endian: bool) -> Result<Vec<MatFileData>, MatFileError> {
    let mut parsed = Vec::new();
    while data.len() != 0 { //TODO: Don't break on parse error, just notify and return successfully parsed data
        let results = parse_single_data_field(data, big_endian)?;
        data = results.0;
        parsed.push(results.1);
    }
    Ok(parsed)
}

/// Parses a single large data element
/// ## Parameters
///  - `data_type`: Data type `u32` of the data element to parse
///  - `data_size`: Size of data element in bytes
///  - `data`: Bytes of the data element to parse
///  - `big_endian`: Set to true if data element should be parsed as big endian
/// ## Return value
/// A `ParseResult` of the parsed data element as a `MatFileData`
/// ## Errors
/// Returns an error if there were any issues during parsing
fn parse_large(data_type: u32, data_size: u32, data: &[u8], big_endian: bool) -> ParseResult<MatFileData> {
    const ALIGNMENT_BYTES: usize = 8;
    // Calculate padding amount
    let data_size = data_size as usize;
    let padding_size = data_size % ALIGNMENT_BYTES;
    let padding_size = if padding_size == 0 { 0 } else { ALIGNMENT_BYTES - padding_size };
    let data_size_padded = if data.len() >= data_size + padding_size {
        debug!("Added {} bytes of padding", padding_size);
        data_size + padding_size
    } else {
        debug!("File end reached, no padding required");
        data_size
    };

    debug!("Eating {} of {} remaining bytes", data_size_padded, data.len());
    let (content, rest_of_file) = take(data, data_size_padded)?;
    let (content, _padding) = take(content, data_size)?;
    let (unprocessed, data) = match data_type as u32 {
        1 => parse_int8(content),
        2 => parse_uint8(content),
        3 => parse_int16(content, big_endian),
        4 => parse_uint16(content, big_endian),
        5 => parse_int32(content, big_endian),
        6 => parse_uint32(content, big_endian),
        7 => parse_single(content, big_endian),
        9 => parse_double(content, big_endian),
        12 => parse_int64(content, big_endian),
        14 => parse_matrix_data(content, big_endian),
        15 => parse_compressed(content, big_endian),
        16 => parse_utf8(content),
        17 => parse_utf16(content, big_endian),
        18 => parse_utf32(content, big_endian),
        extra => {
            let error_message = format!("Unknown datatype: {}", extra);
            error!("{}", error_message);
            fail!(UnknownDatatype(error_message));
        }
    }?;

    if unprocessed.len() > 0 {
        warn!("{} bytes of extra data left at end of data field", unprocessed.len());
        trace!("{:?}", unprocessed);
    }
    Ok((rest_of_file, data))
}

/// Parses a single data field (calls `parse_large` and `parse_small` as required)
/// ## Parameters
///  - `data_raw`: Bytes of data to parse as a single data field
///  - `big_endian`: Set to true if the data field should be parsed as big endian
/// ## Return value
/// A `ParseResult<MatFileData>` of the parsed data field
/// ## Errors
/// Returns an error if there were any issues during parsing
pub fn parse_single_data_field(data_raw: &[u8], big_endian: bool) -> ParseResult<MatFileData> {
    let (data, data_type) = uint32(data_raw)?;
    let (data, data_size) = uint32(data)?;

    debug!("About to process data of type {} with size {}", data_type, data_size);

    let (data, parsed_data) =  if data_type < 2u32.pow(16) {
        parse_large(data_type, data_size, data, big_endian)
    } else {
        let (data, extra) = take(data_raw, 8)?;
        Ok((extra, parse_small(data.try_into().unwrap(), big_endian)?))
    }?;

    let debug = format!("Processed data as {:?}",parsed_data);
    debug!("{}", if debug.len() > 255 {debug[0..255].to_string() + "..."} else {debug});
    Ok((data, parsed_data))
}

#[cfg(test)]
mod tests { //TODO: A LOT more testing
    use crate::test_utils::to_byte_array_u32;
    use super::*;

    #[test]
    fn example_u32() {
        const DATA: [u32;8] = [
            0x06,24,1,2,3,4,5,6
        ];

        let converted_data = to_byte_array_u32(&DATA);
        let (extra, parsed_data) = parse_single_data_field(converted_data.as_slice(), false).unwrap();
        assert_eq!(extra.len(), 0);
        match parsed_data {
            MatFileData::UInt32(data) => assert_eq!(data, vec!(1,2,3,4,5,6)),
            other => panic!("MatFileData::UInt32 parsed as {:?}!", other)
        }
    }

    const VALID_IMAGINARY_MATRIX: [u8;136] = [
        0x0e, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, // Matrix header for 128 byte matrix
        0x06, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, // Array flags
        0x06, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x05, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, // Dimensions array
        0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,
        0x01, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, // Array name
        0x6d, 0x79, 0x5f, 0x61, 0x72, 0x72, 0x61, 0x79, // my_array
        0x09, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, // Double x4
        0x9a, 0x99, 0x99, 0x99, 0x99, 0x99, 0xF1, 0x3F, // 1.1
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x40, // 3.0
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, // 2.0
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x40, // 4.0
        0x09, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, // Double x4 (imaginary part)
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 0x3F, // 1.0
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 0.0
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 0.0
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 0.0
    ];

    fn match_with_matrix_impl(data: &[u8], big_endian: bool) {
        let (extra, parsed) = parse_single_data_field(data, big_endian).unwrap();
        let (extra_subfun, parsed_subfun) = parse_matrix_data(&data[8..], big_endian).unwrap();
        assert_eq!(extra, extra_subfun);
        assert_eq!(parsed, parsed_subfun);
    }

    #[test]
    fn example_imaginary_matrix() {
        match_with_matrix_impl(&VALID_IMAGINARY_MATRIX, false);
    }
}