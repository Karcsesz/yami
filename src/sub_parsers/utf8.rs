use crate::errors::MatError::{InvalidDataType, UTF8ParseError};
use crate::errors::ParseResult;
use crate::fail;
use crate::mat_data::{MatFileData, MatlabString, MatlabStringType};
use crate::sub_parsers::uint8::parse_uint8;

pub fn parse_utf8(data: &[u8]) -> ParseResult<MatFileData> {
    let (extra, data) = parse_uint8(data)?;
    if let MatFileData::UInt8(data) = data {
        Ok((extra, MatFileData::MatlabString(MatlabString{
            subtype: MatlabStringType::UTF8,
            data: match String::from_utf8(data) {
                Ok(data) => data,
                Err(err) => fail!(UTF8ParseError(err))
                }
            }))
        )
    } else {
        fail!(InvalidDataType)
    }
}

//TODO: Testing