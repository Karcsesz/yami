use num_traits::ToPrimitive;
use crate::errors::MatError::TooShort;
use crate::fail;
use crate::errors::ParseResult;

/// Split `data` at `bytes` amount of bytes and return it as a `ParseResult`
/// ## Parameters
///  - `data`: The array to take bytes from
///  - `bytes`: How many bytes should there be in the split
/// ## Return value
/// A `ParseResult` with the main output being the `bytes` bytes of `data` and the remainder field being the rest of `data`
/// ## Errors
/// Returns a `TooShort` error if `data` is smaller than the required `bytes`
pub fn take(data: &[u8], bytes: usize) -> ParseResult<&[u8]> {
    if data.len() < bytes {
        fail!(TooShort)
    }
    Ok(data.split_at(bytes))
}

/// Parse a single `u32` from the start of `data`
/// ## Parameters
///  - `data`: The array to parse the `u32` from
/// ## Return value
/// A `ParseResult` of the parsed `u32` and the rest of `data`
/// ## Errors
/// Returns a `TooShort` error if `data` is less than 4 bytes
pub fn uint32(data: &[u8]) -> ParseResult<u32> {
    if data.len() < 4 {
        fail!(TooShort)
    }
    Ok((&data[4..], u32::from_le_bytes([data[0], data[1], data[2], data[3]])))
}

/// Parse an `i8` array as an UTF-8 string
/// ## Parameters
///  - `data`: The array of `i8` to parse as a string
/// ## Return value
/// The parsed `String`
/// TODO: This function should be refactored to return a `ParseResult`
/// ## Panics
/// If the `i8` values can't be converted to `u8`
pub fn parse_string(data: Vec<i8>) -> String {
    String::from_utf8(data.iter().map(|char| {
        char.to_u8().expect("Non-ASCII string in variable name") //TODO: Propagate errors instead of panicking
    }).collect()).expect("Failed to parse variable name as String")
        .trim_matches('\0').to_string() // Trim extra null bytes
    //TODO: Probably should find the first null terminator and clip the rest off instead of cutting from the end
}

//TODO: Unit tests

/*pub const fn take_static(data: &[u8], bytes: usize) -> Result<(&[u8], &[u8]), MatFileError> {

}*/ //TODO: This should probably be a macro