use crate::errors::ParseResult;
use crate::mat_data::MatFileData;

/// Parses uint16 array with selectable endianness
/// ## Parameters
///  - `data`: Data to parse as array
///  - `big_endian`: Set to true if the data should be parsed as big endian
/// ## Return value
/// Returns a `ParseResult` with the parsed data as a `MatFileData::UInt16`,
/// and the remaining bytes optionally set to the last byte of the input data
/// if there are extra bytes at the end of the array.
pub fn parse_uint16(data: &[u8], big_endian: bool) -> ParseResult<MatFileData>{
    // Chunk the data
    let data = data.chunks_exact(2);

    // If there's an extra byte, put it away for later
    let remainder = data.remainder();

    // Select parser for the correct endianness
    let parser = if big_endian {
        u16::from_be_bytes
    } else {
        u16::from_le_bytes
    };

    // Parse every chunk with the selected parser
    let parsed = MatFileData::UInt16(data.map(
    |chunk| {
            parser([chunk[0], chunk[1]])
        }
    ).collect());

    Ok((remainder, parsed))
}

#[cfg(test)]
mod tests {
    use crate::test_utils::to_byte_array_u16;
    use super::*;

    /// Tests parsing a single element
    #[test]
    fn single_element() {
        const DATA: [u16;1] = [
            0x1234u16
        ];

        let byte_array = to_byte_array_u16(&DATA);
        let (extra_data, parsed) = parse_uint16(byte_array.as_slice(), false).unwrap();
        assert_eq!(parsed, MatFileData::UInt16(Vec::from(DATA)));
        assert_eq!(extra_data.len(), 0);
    }

    /// Tests parsing every possible u16 element
    #[test]
    fn every_element() {
        let data = Vec::from_iter(u16::MIN..u16::MAX);

        let byte_array = to_byte_array_u16(&data);
        let (extra_data, parsed) = parse_uint16(byte_array.as_slice(), false).unwrap();
        assert_eq!(parsed, MatFileData::UInt16(data));
        assert_eq!(extra_data.len(), 0);
    }

    /// Tests parsing many elements (123456 elements)
    #[test]
    fn many_elements() {
        let data = Vec::from_iter((u16::MIN..u16::MAX).cycle().take(123456));

        let byte_array = to_byte_array_u16(&data);
        let (extra_data, parsed) = parse_uint16(byte_array.as_slice(), false).unwrap();
        assert_eq!(parsed, MatFileData::UInt16(data));
        assert_eq!(extra_data.len(), 0);
    }

    /// Tests what happens if there isn't enough data to form an element
    #[test]
    fn not_enough_data() {
        const DATA: [u8;1] = [
            0x12
        ];

        let parsed = parse_uint16(&DATA, false).unwrap();
        assert_eq!(parsed.0, DATA);
        assert_eq!(parsed.1, MatFileData::UInt16(vec![]));
    }

    /// Tests what happens if there isn't enough data to form an element and the function is called with `big_endian` set to true
    #[test]
    fn not_enough_data_reverse_endian() {
        const DATA: [u8;1] = [
            0x12
        ];

        let parsed = parse_uint16(&DATA, true).unwrap();
        assert_eq!(parsed.0, DATA);
        assert_eq!(parsed.1, MatFileData::UInt16(vec![]));
    }

    /// Tests what happens if there's a single unparseable byte left at the end of the data input
    #[test]
    fn too_short_element() {
        const DATA: [u8;3] = [
            0x01, 0x23, 0x45
        ];

        let (extra_data, parsed) = parse_uint16(&DATA, false).unwrap();

        assert_eq!(extra_data.len(), 1);
        assert_eq!(parsed, MatFileData::UInt16(vec!(u16::from_le(0x2301u16))));
    }

    /// Tests what happens if there's a single unparseable byte left at the end of the data input with `big_endian` set to true
    #[test]
    fn too_short_element_reverse_endian() {
        const DATA: [u8;3] = [
            0x23, 0x01, 0x45
        ];

        let (extra_data, parsed) = parse_uint16(&DATA, true).unwrap();

        assert_eq!(extra_data.len(), 1);
        assert_eq!(parsed, MatFileData::UInt16(vec!(0x2301u16)));
    }
}