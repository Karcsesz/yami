use log::{debug, trace, warn};
use crate::errors::MatFileError;
use crate::mat_data::MatFileData;
use crate::sub_parsers::parse_large;
use crate::sub_parsers::parser_utils::take;

pub fn parse_small(data: &[u8; 8], big_endian: bool) -> Result<MatFileData, MatFileError> {
    // Remap cut header data
    // |        type       |       size        |
    // |----|----|----|----|----|----|----|----| ||
    // |----|----|----|----|----|----|----|----| \/
    // |   size  |  type   |       data        |
    let (small_data_header, extra) = take(data,4usize)?;
    let size = u16::from_le_bytes([small_data_header[2], small_data_header[3]]) as u32; //TODO: How would this work with endian swapping?
    let data_type = u16::from_le_bytes([small_data_header[0], small_data_header[1]]) as u32;
    let (small_data_bytes, _extra) = take(extra, size as usize)?;
    debug!("Small data element with data type of {} and size of {}", data_type, size);
    trace!("{:?}", small_data_bytes);

    // Parse remapped data as large element
    let (leftover, parsed) = parse_large(data_type, size, small_data_bytes, big_endian)?;
    if leftover.len() > 0 {
        warn!("{} bytes of data left over from small data element", leftover.len());
        trace!("{:?}", leftover);
    }

    Ok(parsed)
}