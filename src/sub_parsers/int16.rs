use crate::errors::ParseResult;
use crate::mat_data::MatFileData;

/// Parses int16 array with selectable endianness
/// ## Parameters
///  - `data`: Data to parse as array
///  - `big_endian`: Set to true if the data should be parsed as big endian
/// ## Return value
/// Returns a `ParseResult` with the parsed data as a `MatFileData::Int16`,
/// and the remaining bytes set to the last byte of the input data
/// if there are extra bytes at the end of the array.
pub fn parse_int16(data: &[u8], big_endian: bool) -> ParseResult<MatFileData>{
    // Chunk data
    let data = data.chunks_exact(2);

    // If there are leftover bytes, put them away for later
    let remainder = data.remainder();

    // Select parser based on endianness
    let parser = if big_endian {
        i16::from_be_bytes
    } else {
        i16::from_le_bytes
    };

    // Parse every chunk
    let parsed = MatFileData::Int16(data.map(
    |chunk| {
          parser([chunk[0], chunk[1]])
      }
    ).collect());

    Ok((remainder, parsed))
}

#[cfg(test)]
mod tests {
    use crate::test_utils::to_byte_array_i16;
    use super::*;

    /// Tests parsing a single element
    #[test]
    fn single_element() {
        const DATA: [i16;1] = [
            0x1234i16
        ];

        let byte_array = to_byte_array_i16(&DATA);
        let (extra_data, parsed) = parse_int16(byte_array.as_slice(), false).unwrap();
        assert_eq!(parsed, MatFileData::Int16(Vec::from(DATA)));
        assert_eq!(extra_data.len(), 0);
    }

    /// Tests parsing a negative element
    #[test]
    fn single_negative_element() {
        const DATA: [i16;1] = [
            -0x1234i16
        ];

        let byte_array = to_byte_array_i16(&DATA);
        let (extra_data, parsed) = parse_int16(byte_array.as_slice(), false).unwrap();
        assert_eq!(parsed, MatFileData::Int16(Vec::from(DATA)));
        assert_eq!(extra_data.len(), 0);
    }

    /// Tests what happens if there isn't enough data to form an element
    #[test]
    fn not_enough_data() {
        const DATA: [u8;1] = [
            0x12
        ];

        let parsed = parse_int16(&DATA, false).unwrap();
        assert_eq!(parsed.0, DATA);
        assert_eq!(parsed.1, MatFileData::Int16(vec![]));
    }

    /// Tests what happens if there isn't enough data to form an element with `big_endian` set to true
    #[test]
    fn not_enough_data_reverse_endian() {
        const DATA: [u8;1] = [
            0x12
        ];

        let parsed = parse_int16(&DATA, true).unwrap();
        assert_eq!(parsed.0, DATA);
        assert_eq!(parsed.1, MatFileData::Int16(vec![]));
    }

    /// Tests what happens if there are leftover bytes at the end of the array
    #[test]
    fn too_short_element() {
        const DATA: [u8;3] = [
            0x01, 0x23, 0x45
        ];

        let (extra_data, parsed) = parse_int16(&DATA, false).unwrap();

        assert_eq!(extra_data.len(), 1);
        assert_eq!(parsed, MatFileData::Int16(vec!(0x2301i16)));
    }

    /// Tests what happens if there are leftover bytes at the end of the array with `big_endian` set to true
    #[test]
    fn too_short_element_reverse_endian() {
        const DATA: [u8;3] = [
            0x23, 0x01, 0x45
        ];

        let (extra_data, parsed) = parse_int16(&DATA, true).unwrap();

        assert_eq!(extra_data.len(), 1);
        assert_eq!(parsed, MatFileData::Int16(vec!(0x2301i16)));
    }
}