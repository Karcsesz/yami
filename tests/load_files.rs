extern crate core;

use std::fs::File;
use std::ops::BitAnd;
use num_traits::ToPrimitive;
use yami::{decode, load, open};
use yami::mat_data::{Contents, MatFileData, MatlabNumericArray};

fn validate_u16_array(contents: Contents) {
    let (header, body) = (contents.header, contents.body);
    assert_eq!(header.version, 0x0100);
    assert!(header.subsystem_data_offset.iter().all(|byte| byte == &0));
    assert!(!header.needs_endian_swapping);
    assert_eq!(body.len(), 1);
    let body = body.get(0).unwrap();
    if let MatFileData::MatlabNumericArray(data) = body {
        assert_eq!(data.sizes, vec![1,2000]);
        assert_eq!(data.name, "numbers");
        assert_eq!(data.array_flags[0].bitand(0b0111000011111111), 6);
        // array_flags[1] is undefined
        assert_eq!(data.imaginary, None);
        if let MatFileData::UInt16(contents) = &*data.real {
            assert_eq!(contents.len(), 2000);
            assert!(contents.iter().enumerate().all(|(index, element)| element.to_usize().unwrap() == index + 1))
        } else {
            panic!("Internal datatype is {:?}", *data.real);
        }
    } else {
        panic!("Type of data element is {:?}", body);
    }
}

#[test]
fn open_u16_array() {
    let converted = open("test_files/u16_array.mat").unwrap();
    assert_eq!(converted.header.text, "MATLAB 5.0 MAT-file, Platform: PCWIN64, Created on: Thu Nov 24 15:55:16 2022                                        ");
    validate_u16_array(converted);
}

#[test]
fn load_u16_array() {
    let file = File::open("test_files/u16_array.mat").unwrap();
    let converted = load(file).unwrap();
    assert_eq!(converted.header.text, "MATLAB 5.0 MAT-file, Platform: PCWIN64, Created on: Thu Nov 24 15:55:16 2022                                        ");
    validate_u16_array(converted);
}

#[test]
fn convert_u16_array() {
    let file_data = include_bytes!("../test_files/u16_array.mat");
    let converted = decode(file_data).unwrap();
    assert_eq!(converted.header.text, "MATLAB 5.0 MAT-file, Platform: PCWIN64, Created on: Thu Nov 24 15:55:16 2022                                        ");
    validate_u16_array(converted);
}

#[test]
fn open_u16_array_compressed() {
    let converted = open("test_files/u16_array_compressed.mat").unwrap();
    assert_eq!(converted.header.text, "MATLAB 5.0 MAT-file, Platform: PCWIN64, Created on: P nov. 25 11:22:51 2022                                         ");
    validate_u16_array(converted);
}

#[test]
fn load_u16_array_compressed() {
    let file = File::open("test_files/u16_array_compressed.mat").unwrap();
    let converted = load(file).unwrap();
    assert_eq!(converted.header.text, "MATLAB 5.0 MAT-file, Platform: PCWIN64, Created on: P nov. 25 11:22:51 2022                                         ");
    validate_u16_array(converted);
}

#[test]
fn convert_u16_array_compressed() {
    let file_data = include_bytes!("../test_files/u16_array_compressed.mat");
    let converted = decode(file_data).unwrap();
    assert_eq!(converted.header.text, "MATLAB 5.0 MAT-file, Platform: PCWIN64, Created on: P nov. 25 11:22:51 2022                                         ");
    validate_u16_array(converted);
}

#[test]
fn load_struct() {
    let converted = open("test_files/u16_array_compressed.mat").unwrap();
    assert_eq!(converted.header.text, "MATLAB 5.0 MAT-file, Platform: PCWIN64, Created on: P nov. 25 11:22:51 2022                                         ");
    assert_eq!(converted.header.version, 0x0100);
    assert_eq!(converted.header.needs_endian_swapping, false);
    assert_eq!(converted.body.len(), 1);
    assert_eq!(converted.body[0], MatFileData::MatlabNumericArray(MatlabNumericArray {
        array_flags: [6, 0],
        sizes: vec![1, 2000],
        name: "numbers".to_string(),
        real: Box::new(MatFileData::UInt16(Vec::from_iter(1..2001))),
        imaginary: None,
    }));
}

#[test]
fn load_3dimensional() {
    let converted = open("test_files/3dimensional.mat").unwrap();
    assert_eq!(converted.header.text, "MATLAB 5.0 MAT-file, written by Octave 7.3.0, 2023-01-10 09:51:31 UTC                                               ");
    assert_eq!(converted.header.version, 0x0100);
    assert_eq!(converted.header.needs_endian_swapping, false);
    assert_eq!(converted.body.len(), 1);
    assert_eq!(converted.body[0], MatFileData::MatlabNumericArray(MatlabNumericArray {
        array_flags: [6, 1],
        sizes: vec![3, 3, 2],
        name: "multidim".to_string(),
        real: Box::new(MatFileData::Double(vec![1f64, 4f64, 7f64, 2f64, 5f64, 8f64, 3f64, 6f64, 9f64, 10f64, 13f64, 16f64, 11f64, 14f64, 17f64, 12f64, 15f64, 18f64])),
        imaginary: None
    }))
}

#[test]
fn load_complex_array() {
    let converted = open("test_files/complex_array.mat").unwrap();
    assert_eq!(converted.header.text, "MATLAB 5.0 MAT-file, written by Octave 7.3.0, 2022-12-28 21:44:36 UTC                                               ");
    assert_eq!(converted.header.version, 0x0100);
    assert_eq!(converted.header.needs_endian_swapping, false);
    assert_eq!(converted.body.len(), 1);
    assert_eq!(converted.body[0], MatFileData::MatlabNumericArray(MatlabNumericArray {
        array_flags: [2054, 1],
        sizes: vec![1, 100],
        name: "cplx".to_string(),
        real: Box::new(MatFileData::Double(Vec::from_iter((1..=100).map(|val| val as f64)))),
        imaginary: Some(Box::new(MatFileData::Double(vec![1f64;100]))),
    }))
}

#[test]
fn load_single_complex() {
    let converted = open("test_files/single_complex.mat").unwrap();
    assert_eq!(converted.header.text, "MATLAB 5.0 MAT-file, Platform: MACI64, Created on: Wed Mar 27 21:59:35 2019                                         ");
    assert_eq!(converted.header.version, 0x0100);
    assert_eq!(converted.header.needs_endian_swapping, false);
    assert_eq!(converted.body.len(), 1);
    /*assert_eq!(converted.body[0], MatFileData::MatlabNumericArray(MatlabNumericArray {
        array_flags: [2055, 0],
        sizes: vec![10, 10],
        name: "C".to_string(),

    }))*/ //TODO: Check body
}
