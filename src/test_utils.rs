pub fn to_byte_array_u32(data: &[u32]) -> Vec<u8> {
    let mut res = vec![0;std::mem::size_of_val(data)];
    for i in 0..data.len() {
        res[std::mem::size_of::<u32>()*i..][..std::mem::size_of::<u32>()].copy_from_slice(&data[i].to_le_bytes());
    }
    res
}
pub fn to_byte_array_i16(data: &[i16]) -> Vec<u8> {
    let mut res = vec![0;std::mem::size_of_val(data)];
    for i in 0..data.len() {
        res[std::mem::size_of::<i16>()*i..][..std::mem::size_of::<i16>()].copy_from_slice(&data[i].to_le_bytes());
    }
    res
}
pub fn to_byte_array_u16(data: &[u16]) -> Vec<u8> {
    let mut res = vec![0;std::mem::size_of_val(data)];
    for i in 0..data.len() {
        res[std::mem::size_of::<u16>()*i..][..std::mem::size_of::<u16>()].copy_from_slice(&data[i].to_le_bytes());
    }
    res
}
pub fn to_byte_array_i32(data: &[i32]) -> Vec<u8> {
    let mut res = vec![0;std::mem::size_of_val(data)];
    for i in 0..data.len() {
        res[std::mem::size_of::<i32>()*i..][..std::mem::size_of::<i32>()].copy_from_slice(&data[i].to_le_bytes());
    }
    res
}
pub fn to_byte_array_i64(data: &[i64]) -> Vec<u8> {
    let mut res = vec![0;std::mem::size_of_val(data)];
    for i in 0..data.len() {
        res[std::mem::size_of::<i64>()*i..][..std::mem::size_of::<i64>()].copy_from_slice(&data[i].to_le_bytes());
    }
    res
}
pub fn to_byte_array_f32(data: &[f32]) -> Vec<u8> {
    let mut res = vec![0;std::mem::size_of_val(data)];
    for i in 0..data.len() {
        res[std::mem::size_of::<f32>()*i..][..std::mem::size_of::<f32>()].copy_from_slice(&data[i].to_le_bytes());
    }
    res
}
pub fn to_byte_array_f64(data: &[f64]) -> Vec<u8> {
    let mut res = vec![0;std::mem::size_of_val(data)];
    for i in 0..data.len() {
        res[std::mem::size_of::<f64>()*i..][..std::mem::size_of::<f64>()].copy_from_slice(&data[i].to_le_bytes());
    }
    res
}
pub fn to_byte_array_f64_reverse_endian(data: &[f64]) -> Vec<u8> {
    let mut res = vec![0;std::mem::size_of_val(data)];
    for i in 0..data.len() {
        res[std::mem::size_of::<f64>()*i..][..std::mem::size_of::<f64>()].copy_from_slice(&data[i].to_be_bytes());
    }
    res
}
pub fn to_byte_array_i8(data: &[i8]) -> Vec<u8> { //TODO: Should be way simpler for single byte values
    let mut res = vec![0;std::mem::size_of_val(data)];
    for i in 0..data.len() {
        res[std::mem::size_of::<i8>()*i..][..std::mem::size_of::<i8>()].copy_from_slice(&data[i].to_ne_bytes());
    }
    res
}